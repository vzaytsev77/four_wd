#include "CMotors.h"
#include <util/delay.h>
#include "CRangeMeter.h"
#include <string.h>


CMotors g_motors;


// отключаем моторы слева
ISR(TIMER0_COMPA_vect)
{
    //
    PORTD &= ~(1 << M_ENA);
}


// отключаем моторы справа
ISR(TIMER0_COMPB_vect)
{
    //
    PORTD &= ~(1 << M_ENB);
}


// по переполнению таймера подключаем обе стороны
ISR(TIMER0_OVF_vect)
{
    //
    if ( g_motors.m_running )
    {
        //
        PORTD |= (1 << M_ENA);
        PORTD |= (1 << M_ENB);
    }
}


//
CMotors::CMotors()
    : m_lc_for_dm(DEFAULT_LC_FOR_DM)
    , m_lc_for_5deg(DEFAULT_LC_FOR_DEG)
    , m_lc_counters()
    , m_last_ws_state(0)
    , m_running(false)
    , m_speed_left(DEFAULT_SPEED_LEFT)
    , m_speed_right(DEFAULT_SPEED_RIGHT)
{
    //ctor
    memset( m_lc_counters, 0, sizeof(m_lc_counters) );
}


//
void CMotors::process_interrupt()
{
    //
    if ( m_running )
    {
        // состояние входов, интересуют старшие 4 бита
        uint8_t v_ws_state = PINC >> 4;
        // вычисляем входы, состояние которых изменилось
        uint8_t v_changes = v_ws_state ^ m_last_ws_state;

        // для тех входов
        for ( int8_t i = 0; i < 4; ++i )
        {
            // где состояние изменилось
            if ( 0 != (v_changes & 1) )
            {
                // увеличиваем счётчик для колеса
                ++m_lc_counters[i];
                // увеличиваем счётчик по всем колёсам
                ++m_lc_counter_all;
            }
            // следующий вход
            v_changes >>= 1;
        }
        //
        m_last_ws_state = v_ws_state;
    }
}


//
void CMotors::line_move( bool a_backward, uint8_t a_dm_count )
{
    // проверяем, а можно ли ехать
    if ( MIN_DISTANCE >= (a_backward ? g_range_meter.m_rear : g_range_meter.m_front) )
    {
        //
        PORTB |= (1 << BEEP);
        //
        _delay_ms( 100 );
        //
        PORTB &= ~(1 << BEEP);
    }
    // вычисляем количество переходов уровня для прохождения указанного расстояния
    // * 4 - суммарно для четырёх колёс :)
    uint16_t v_count = a_dm_count * m_lc_for_dm * 4;
    //
    line_move_by_lc( a_backward, v_count );
}


void CMotors::line_move_by_lc( bool a_backward, uint16_t a_lc_count )
{
    //
    PORTA |= (1 << WLED);
    // отключаем моторы
    PORTD &= ~((1 << M_ENA) + (1 << M_ENB));
    // устанавливаем направление
    if ( ! a_backward )
    {
        //
        PORTD |= (1 << M_EN1) + (1 << M_EN4);
        PORTD &= ~((1 << M_EN2) + (1 << M_EN3));
    }
    else
    {
        //
        PORTD |= (1 << M_EN2) + (1 << M_EN3);
        PORTD &= ~((1 << M_EN1) + (1 << M_EN4));
    }
    // зачищаем счётчик
    m_lc_counter_all = 0;
    // запоминаем текущее состояние
    m_last_ws_state = PINC >> 4;
    //
    m_running = true;
    // включаем моторы
    PORTD |= (m_speed_left ? (1 << M_ENA) : 0) + (m_speed_right ? (1 << M_ENB) : 0);
    // теперь таймер и прерывания в зависимости от скорости
    TCCR0B = 0;
    TCCR0A = 0; // Normal mode
    TCNT0 = 0;
    TIMSK0 = 0;
    TIFR0 = (1 << OCF0B) + (1 << OCF0A) + (1 << TOV0);
    //
    if ( m_speed_left > 0 && m_speed_left < 255 )
    {
        //
        TIMSK0 |= (1 << OCIE0A) + (1 << TOIE0);
    }
    //
    if ( m_speed_right > 0 && m_speed_right < 255 )
    {
        //
        TIMSK0 |= (1 << OCIE0B) + (1 << TOIE0);
    }
    //
    if ( 0 != TIMSK0 )
    {
        //
        OCR0A = m_speed_left;
        OCR0B = m_speed_right;
        //
        TCCR0B = (1 << CS01);
    }
    // ждём
    while ( m_lc_counter_all < a_lc_count )
    {
        if ( MIN_DISTANCE >= (a_backward ? g_range_meter.m_rear : g_range_meter.m_front) )
        {
            //
            TCCR0B = 0;
            TIFR0 = (1 << OCF0B) + (1 << OCF0A) + (1 << TOV0);
            //
            m_running = false;
            // отключаем моторы
            PORTD &= ~((1 << M_ENA) + (1 << M_ENB));
            //
            PORTB |= (1 << BEEP);
            //
            _delay_ms( 100 );
            //
            PORTB &= ~(1 << BEEP);
            //
            break;
        }
    }
    //
    TCCR0B = 0;
    TIFR0 = (1 << OCF0B) + (1 << OCF0A) + (1 << TOV0);
    //
    m_running = false;
    // отключаем моторы
    PORTD &= ~((1 << M_ENA) + (1 << M_ENB));
    //
    _delay_ms( 10 );
    //
    PORTA &= ~(1 << WLED);
}


//
void CMotors::turnaround( bool a_ccw, uint16_t a_5deg_count )
{
    uint16_t v_count = a_5deg_count * m_lc_for_5deg * 4;
    //
    turnaround_by_lc( a_ccw, v_count );
}


//
void CMotors::turnaround_by_lc( bool a_ccw, uint16_t a_lc_count )
{
    // отключаем моторы
    PORTD &= ~((1 << M_ENA) + (1 << M_ENB));
    // устанавливаем направление
    if ( ! a_ccw )
    {
        //
        PORTD |= (1 << M_EN2) + (1 << M_EN4);
        PORTD &= ~((1 << M_EN1) + (1 << M_EN3));
    }
    else
    {
        //
        PORTD |= (1 << M_EN1) + (1 << M_EN3);
        PORTD &= ~((1 << M_EN2) + (1 << M_EN4));
    }
    // зачищаем счётчик
    m_lc_counter_all = 0;
    // запоминаем текущее состояние
    m_last_ws_state = PINC >> 4;
    //
    m_running = true;
    // включаем моторы
    PORTD |= (1 << M_ENA) + (1 << M_ENB);
    // ждём
    while ( m_lc_counter_all < a_lc_count );
    //
    m_running = false;
    // отключаем моторы
    PORTD &= ~((1 << M_ENA) + (1 << M_ENB));
    //
    _delay_ms( 10 );
}

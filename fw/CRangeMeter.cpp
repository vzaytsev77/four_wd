#include "CRangeMeter.h"
#include <util/delay.h>


CRangeMeter g_range_meter;


ISR(TIMER1_OVF_vect)
{
    //
    TCCR1B = 0;
    TCNT1H = 0;
    TCNT1L = 0;
    TIMSK1 = 0;
    //
    switch (g_range_meter.m_state)
    {
    case CRangeMeter::ers_wait_idle_front:
    case CRangeMeter::ers_wait_idle_rear:
        // подождали, проверим готовность
        if ( 0 != (PINC & (1 << US_OUT)) )
        {
            // можно запускать измерения
            if ( CRangeMeter::ers_wait_idle_front == g_range_meter.m_state )
            {
                g_range_meter.m_state = CRangeMeter::ers_make_start_front;
                // старт импульса
                PORTC |= (1 << US_F);
            }
            else
            {
                g_range_meter.m_state = CRangeMeter::ers_make_start_rear;
                // старт импульса
                PORTC |= (1 << US_R);
            }
            // ждём 13 мкс
            OCR1AH = 0;
            OCR1AL = 12;
            //
            TIFR1 = (1 << OCF1A);
            TIMSK1 = (1 << OCIE1A);
            TCCR1B = (1 << CS11);
        }
        else
        {
            // не готово, меняем канал и ждём ещё
            g_range_meter.m_state =
                CRangeMeter::ers_wait_idle_front == g_range_meter.m_state
                ? CRangeMeter::ers_wait_idle_rear
                : CRangeMeter::ers_wait_idle_front
                ;
            //
            TIMSK1 = (1 << TOIE1);
            TCCR1B = (1 << CS10);
        }
        break;

    case CRangeMeter::ers_wait_echo_front:
    case CRangeMeter::ers_wait_echo_rear:
        // не дождались эхо, переключаемся на следующее измерение
        g_range_meter.m_state =
            CRangeMeter::ers_wait_echo_front == g_range_meter.m_state
            ? CRangeMeter::ers_wait_idle_rear
            : CRangeMeter::ers_wait_idle_front
            ;
        //
        TIMSK1 = (1 << TOIE1);
        TCCR1B = (1 << CS11);
        break;

    default:
        break;
    }
}


ISR(TIMER1_COMPA_vect)
{
    //
    TCCR1B = 0;
    TCNT1H = 0;
    TCNT1L = 0;
    TIMSK1 = 0;
    //
    switch (g_range_meter.m_state)
    {
    case CRangeMeter::ers_make_start_front:
        // завершаем стартовый импульс
        PORTC &= ~(1 << US_F);
        // теперь надо ждать начала импульса ответа
        g_range_meter.m_state = CRangeMeter::ers_wait_echo_front;
        //
        TIFR1 = (1 << TOV1);
        TIMSK1 = (1 << TOIE1);
        TCCR1B = (1 << CS11);
        //
        break;

    case CRangeMeter::ers_make_start_rear:
        // завершаем стартовый импульс
        PORTC &= ~(1 << US_R);
        // теперь надо ждать начала импульса ответа
        g_range_meter.m_state = CRangeMeter::ers_wait_echo_rear;
        //
        TIFR1 = (1 << TOV1);
        TIMSK1 = (1 << TOIE1);
        TCCR1B = (1 << CS11);
        //
        break;

    // обломилось измерение, слишком далеко
    case CRangeMeter::ers_wait_result_front:
        //
        g_range_meter.m_front = 255;
        //
        g_range_meter.m_state = CRangeMeter::ers_wait_idle_rear;
        //
        TIFR1 = (1 << TOV1);
        TIMSK1 = (1 << TOIE1);
        TCCR1B = (1 << CS11);
        //
        break;

    case CRangeMeter::ers_wait_result_rear:
        //
        g_range_meter.m_rear = 255;
        //
        g_range_meter.m_state = CRangeMeter::ers_wait_idle_front;
        //
        TIFR1 = (1 << TOV1);
        TIMSK1 = (1 << TOIE1);
        TCCR1B = (1 << CS11);
        //
        break;

    default:
        break;
    }
}


//
CRangeMeter::CRangeMeter()
    : m_front(0)
    , m_rear(0)
    , m_state(ers_undefined)
{
    //ctor
}


// запуск процесса измерения расстояний
void CRangeMeter::init()
{
    //
    TCCR1B = 0;
    //
    TCCR1A = 0;
    //
    TIFR1 = (1 << TOV1) + (1 << OCF1A);
    //
    cli();
    TCNT1H = 0;
    TCNT1L = 0;
    OCR1AH = 0;
    OCR1AL = 12;
    sei();
    //
    if ( 0 != (PINC & (1 << US_OUT)) )
    {
        // можно сразу стартовый импульс
        m_state = ers_make_start_front;
        // старт импульса
        PORTC |= (1 << US_F);
        // ждём 13 мкс
        TIFR1 = (1 << OCF1A);
        TIMSK1 = (1 << OCIE1A);
        TCCR1B = (1 << CS11);
    }
    else
    {
        //
        m_state = ers_wait_idle_front;
        //
        TIMSK1 = (1 << TOIE1);
        TCCR1B = (1 << CS10);
    }
    //
    _delay_ms( 200 );
}


// обработка прерывания по смене уровня
void CRangeMeter::process_interrupt()
{
    //
    switch ( m_state )
    {
    case ers_wait_result_front:
        if ( 0 != (PINC & (1 << US_OUT)) )
        {
            // измерение завершено
            TCCR1B = 0;
            TIFR1 |= (1 << TOV1);
            //
            uint16_t v_tm = TCNT1L | (((uint16_t)TCNT1H) << 8);
            //
            uint16_t v_cm = (uint16_t)(((int32_t)v_tm * 1178) / 65536);
            //
            m_front =
                v_cm <= 255
                ? (uint8_t)v_cm
                : 255
                ;
            // переключаемся на измерение сзади
            m_state = ers_make_start_rear;
            // выдаём стартовый импульс
            PORTC |= (1 << US_R);
            // ждём 13 мкс
            TCNT1H = 0;
            TCNT1L = 0;
            OCR1AH = 0;
            OCR1AL = 12;
            //
            TIFR1 = (1 << OCF1A);
            TIMSK1 = (1 << OCIE1A);
            TCCR1B = (1 << CS11);
        }
        break;

    case ers_wait_result_rear:
        if ( 0 != (PINC & (1 << US_OUT)) )
        {
            // измерение завершено
            TCCR1B = 0;
            TIFR1 |= (1 << TOV1);
            //
            uint16_t v_tm = TCNT1L | (((uint16_t)TCNT1H) << 8);
            //
            uint16_t v_cm = (uint16_t)(((int32_t)v_tm * 1178) / 65536);
            //
            m_rear =
                v_cm <= 255
                ? (uint8_t)v_cm
                : 255;
            // переключаемся на измерение сзади
            m_state = ers_make_start_front;
            // выдаём стартовый импульс
            PORTC |= (1 << US_F);
            // ждём 13 мкс
            TCNT1H = 0;
            TCNT1L = 0;
            OCR1AH = 0;
            OCR1AL = 12;
            //
            TIFR1 = (1 << OCF1A);
            TIMSK1 = (1 << OCIE1A);
            TCCR1B = (1 << CS11);
        }
        break;

    case ers_wait_echo_front:
    case ers_wait_echo_rear:
        if ( 0 == (PINC & (1 << US_OUT)) )
        {
            // можно ждать результат
            TCCR1B = 0;
            TCNT1H = 0;
            TCNT1L = 0;
            TIFR1 |= (1 << TOV1);
            //
            m_state =
                ers_wait_echo_front == m_state
                ? ers_wait_result_front
                : ers_wait_result_rear
                ;
            //
            OCR1AH = 0xFF;
            OCR1AL = 0;
            //
            TIFR1 |= (1 << OCF1A);
            TIMSK1 = (1 << OCIE1A);
            TCCR1B = (1 << CS11);
        }
        break;

    default:
        break;
    }
}

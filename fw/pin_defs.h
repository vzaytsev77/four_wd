#ifndef PIN_DEFS_H_INCLUDED
#define PIN_DEFS_H_INCLUDED

#include <avr/io.h>
#include <avr/pgmspace.h>


#define write_string_P(__s)         write_string_p(P(__s))

// PORTA
#define VMETER          PORT0
#define WLED            PORT1
#define PWRON           PORT2
#define VSEL            PORT3
#define BT_1            PORT4
#define BT_2            PORT5
#define BT_3            PORT6
#define BT_4            PORT7


// PORTB
#define BEEP            PORT0
#define LCD_RW          PORT1
#define LCD_RS          PORT2
#define LCD_E           PORT3
#define LCD_D4          PORT4
#define LCD_D5          PORT5
#define LCD_D6          PORT6
#define LCD_D7          PORT7


// PORTC
#define US_OUT          PORT0
#define US_F            PORT1
#define US_R            PORT2
#define BT_STATE        PORT3
#define WS_1            PORT4
#define WS_2            PORT5
#define WS_3            PORT6
#define WS_4            PORT7


// PORTD
#define BT_TX           PORT0
#define BT_RX           PORT1
#define M_ENA           PORT2
#define M_EN1           PORT3
#define M_EN2           PORT4
#define M_EN3           PORT5
#define M_EN4           PORT6
#define M_ENB           PORT7

#ifndef P
#define P(s) ({static const char PROGMEM c[] = s;c;})
#endif

#endif // PIN_DEFS_H_INCLUDED

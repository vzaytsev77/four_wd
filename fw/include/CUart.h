#ifndef CUART_H
#define CUART_H

#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "../pin_defs.h"
#include <util/delay.h>


#define BAUD_RATE           38400


class CUart
{
    public:
        CUart();
        // возвращает количество байт, доступных для записи
        // в буфере передачи
        uint8_t tx_avail_depth();
        // возвращает количество записанных в буфер передачи байтов
        // если a_end_ptr != nullptr, устанавливает указатель на
        // первый непереданный символ
        uint8_t tx_write( uint8_t * a_src, uint8_t ** a_end_ptr, uint8_t a_count );
        //
        void write_string( const char * a_str );
        void write_string_p( const char * a_str );
        //
        void write_char( char a_char );
        // возвращает true, если прочитан символ
        bool read_char( char & a_dst );
        // возвращает true, если прочитана строка (среди символов попался \n)
        bool read_string( char * a_dst, uint8_t & a_readed, uint8_t a_limit );

        // буфер передачи
        uint8_t m_tx_ring[256];
        uint8_t m_tx_head;
        uint8_t m_tx_tail;
        bool    m_tx_empty;
        bool    m_tx_full;
        // буфер приёма
        uint8_t m_rx_ring[256];
        uint8_t m_rx_head;
        uint8_t m_rx_tail;
        bool    m_rx_empty;
        bool    m_rx_full;
        // буфер строки
        char    m_line[512];
        int     m_line_idx;
};


extern CUart g_uart;

#endif // CUART_H

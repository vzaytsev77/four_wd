#ifndef CLCD16X2_H
#define CLCD16X2_H

#include <avr/io.h>
#include <util/delay.h>
#include "../pin_defs.h"


class CLcd16x2
{
    public:
        CLcd16x2();
        void init();
        void write_string( const char * a_str );
        void write_string_p( const char * a_str );

    private:
        void write_half_cmd( uint8_t a_four_bits );
        void write_half_data( uint8_t a_four_bits );
        void write_cmd( uint8_t a_byte );
        void write_data( uint8_t a_byte );
        void wait_busy();
        void write_char( char a_char );
};

#endif // CLCD16X2_H

#ifndef CMOTORS_H
#define CMOTORS_H

#include "pin_defs.h"
#include <avr/interrupt.h>


// значение по-умолчанию, количество переходов уровня на 1 дм
#define DEFAULT_LC_FOR_DM   24
// значение по-умолчанию, количество переходов уровня на 5 градусов
#define DEFAULT_LC_FOR_DEG  8
// минимальная дистанция, см
#define MIN_DISTANCE        20

//
#define DEFAULT_SPEED_LEFT  255
#define DEFAULT_SPEED_RIGHT 240
#define MIN_SPEED           180


class CMotors
{
    public:
        CMotors();

        // линейное движение, false - вперёд,true - назад; на указанное число дециметров
        void line_move( bool a_backward, uint8_t a_dm_count );
        //
        void line_move_by_lc( bool a_backward, uint16_t a_lc_count );
        // разворот на месте, false - по часовой стрелке, true - против часовой стрелки
        // и на сколько градусов = 5 * a_5deg_count
        void turnaround( bool a_ccw, uint16_t a_5deg_count );
        //
        void turnaround_by_lc( bool a_ccw, uint16_t a_lc_count );
        //
        void process_interrupt();


        //
        uint8_t m_lc_for_dm;
        //
        uint8_t m_lc_for_5deg;
        //
        uint16_t m_lc_counters[4];
        //
        volatile uint16_t m_lc_counter_all;
        // последнее состояние входов
        uint8_t m_last_ws_state;
        //
        uint8_t m_running;
        // скорости, от 0 до 255
        uint8_t m_speed_left;
        uint8_t m_speed_right;
};


extern CMotors g_motors;

#endif // CMOTORS_H

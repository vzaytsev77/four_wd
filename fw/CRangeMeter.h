#ifndef CRANGEMETER_H
#define CRANGEMETER_H

#include "pin_defs.h"
#include <avr/interrupt.h>




class CRangeMeter
{
    public:
        CRangeMeter();
        //
        void init();
        //

        // расстояния в см до препятствий
        // последние измерянные
        volatile uint8_t m_front;
        volatile uint8_t m_rear;

        //
        void process_interrupt();

        enum e_rm_states
            {
                ers_undefined
              , ers_wait_idle_front     // ждать 0 на линии сигнала дальномера для переднего датчика
              , ers_make_start_front    // выдать сигнал старт для переднего датчика
              , ers_wait_echo_front     // ждать начало сигнала (1) дальномера для переднего датчика
              , ers_wait_result_front   // ждать завершения сигнала дальномера для переднего датчика

              , ers_wait_idle_rear      // ждать 0 на линии сигнала дальномера для заднего датчика
              , ers_make_start_rear     // выдать сигнал старт для заднего датчика
              , ers_wait_echo_rear      // ждать начало сигнала (1) дальномера для заднего датчика
              , ers_wait_result_rear    // ждать завершения сигнала дальномера для заднего датчика
            };
        //
        e_rm_states m_state;
};


extern CRangeMeter g_range_meter;

#endif // CRANGEMETER_H

/*
 */

#include "pin_defs.h"
#include "include/CUart.h"
#include "include/CLcd16x2.h"
#include "CMotors.h"
#include "CRangeMeter.h"
#include <util/delay.h>
#include <stdio.h>


#define POWER_9V_MIN            119
#define POWER_12V_MIN           158
#define BT_TIMEOUT              5 // * 10 мс

uint8_t g_power_min = 0;


uint8_t g_adc_count = 0;
uint16_t g_adc_sum = 0;
uint8_t g_adc_result = 0;

uint8_t g_buttons = 0;
uint8_t g_bt_timeouts[4];

//
CLcd16x2 g_lcd;
//
CUart g_uart;
//
char g_rx_line[65];
uint8_t g_rx_line_readed = 0;


//
ISR(ADC_vect)
{
    //
    g_adc_sum += ADCL + (((uint16_t)ADCH) << 8);
    //
    ++g_adc_count;
    //
    if ( g_adc_count >= 64 )
    {
        //
        g_adc_result = (uint8_t)(g_adc_sum >> 8);
        //
        g_adc_count = 0;
        //
        g_adc_sum = 0;
    }
    //
    ADCSRA |= (1 << ADSC);
}


void init_hardware();

char g_line[32];


void do_command( char * a_cmd, int a_param );


int main(void)
{
    char v_char[4];
    int v_param;

    // настройки железа
    init_hardware();
    //
    sei();
    //
    _delay_ms( 500 );
    //
    g_lcd.init();
    //
    g_uart.write_string( "Hello from supercar\r\n" );
    //
    _delay_ms( 500 );
    // проверим напряжение питания
    if ( g_adc_result < g_power_min )
    {
        // работать низзя, мигаем изредка фарами
        for (;;)
        {
            //
            PORTA |= (1 << WLED);
            _delay_ms( 50 );
            PORTA &= ~(1 << WLED);
            _delay_ms( 200 );
        }
    }

    //
    g_range_meter.init();
    // проедем немного
    PORTA |= (1 << PWRON);
//    g_motors.line_move( false, 2 );
//    g_motors.line_move( true, 2 );
    g_motors.turnaround( false, 10 );
    g_motors.turnaround( true, 10 );
    PORTA &= ~(1 << PWRON);

    //
    while(1)
    {
        //
        ::sprintf( g_line, "\x01%3u cm, %3u cm", g_range_meter.m_front, g_range_meter.m_rear );
        //
        g_lcd.write_string( g_line );
        //
        uint8_t v_volt =  (uint8_t)((((uint32_t)g_adc_result) * 43767) / 65536);
        ::sprintf( g_line, "\x02%2u.%1u B          ", v_volt / 10u, v_volt % 10u );
        //
        g_lcd.write_string( g_line );
        //
        if ( g_uart.read_string( g_rx_line, g_rx_line_readed, sizeof(g_rx_line) - g_rx_line_readed - 1 ) )
        {
            // прочитали строку
            if ( 2 == sscanf( g_rx_line, "%3s%d", v_char, &v_param ) )
            {
                //
                do_command( v_char, v_param );
            }
            else
            {
                //
                g_uart.write_string_p( P("unexpected command format\r\n") );
                g_uart.write_string( g_rx_line );
                g_uart.write_string_p( P("\r\n") );
            }
        }
        else
        {
            //
            if ( (sizeof(g_rx_line) - 1) == g_rx_line_readed )
            {
                // читаем снова
                g_rx_line_readed = 0;
            }
        }
        //
        _delay_ms( 200 );
    }

    return 0;
}


//
void init_hardware()
{
    //
    d_bt_timeouts[0] = 0;
    d_bt_timeouts[1] = 0;
    d_bt_timeouts[2] = 0;
    d_bt_timeouts[3] = 0;
    // порты
    // PORTA
    PORTA = 0;
    DDRA = (1 << WLED) + (1 << PWRON);
    g_buttons = ~(PINA);
    // PORTB
    PORTB = 0;
    DDRB = 0xFF;
    PORTB = (1 << LCD_RW);
    // PORTC
    PORTC = (1 << BT_STATE);
    DDRC = (1 << US_R) + (1 << US_F);
    // PORTD
    PORTD = 0;
    DDRD = (1 << M_ENA) + (1 << M_EN1) + (1 << M_EN2) + (1 << M_EN3) + (1 << M_EN4) + (1 << M_ENB);
    //
    ADMUX = (1 << REFS0);
    //
    ADCSRA = (1 << ADEN) + (1 << ADSC) + (1 << ADPS2) + (1 << ADPS1);
    //
    while ( 0 != (ADCSRA & (1 << ADSC) ) );
    //
    ADCSRA = (1 << ADEN) + (1 << ADSC) + (1 << ADIE) + (1 << ADPS2) + (1 << ADPS1);
    // минимальное напряжение питания
    g_power_min =
        ( 0 == (PINA & (1 << VSEL)) )
        ? POWER_9V_MIN
        : POWER_12V_MIN;
    // прерывания для датчиков вращения колёс
    PCMSK2 = (1 << PCINT23) + (1 << PCINT22) + (1 << PCINT21) + (1 << PCINT20) + (1 << PCINT16);
    PCIFR |= (1 << PCIF2);
    PCICR |= (1 << PCIE2);
    // таймер для клавиатуры 7372800/1024/72 = 100 Гц
    TCCR2A = (1 << WGM21);
    OCR2A = 71;
    TIFR2 = (1 << OCFA);
    TIMSK2 = (1 << OCIEA);
    TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);
}


const uint32_t c_cmd_forward = 0x00647766; // fwd
const uint32_t c_cmd_backward = 0x006B6362; // bck
const uint32_t c_cmd_rotate_cw = 0x00776372; // rcw
const uint32_t c_cmd_rotate_ccw = 0x00636372; // rcc
const uint32_t c_cmd_speed = 0x00647073; // spd
const uint32_t c_cmd_speed_left = 0x006C7073; // spl
const uint32_t c_cmd_speed_right = 0x00727073; // spr

//
void do_command( char * a_cmd, int a_param )
{
    //
    switch ( *((uint32_t *)a_cmd) )
    {
        case c_cmd_forward:
            PORTA |= (1 << PWRON);
            g_motors.line_move( false, a_param );
            PORTA &= ~(1 << PWRON);
            break;

        case c_cmd_backward:
            PORTA |= (1 << PWRON);
            g_motors.line_move( true, a_param );
            PORTA &= ~(1 << PWRON);
            break;

        case c_cmd_rotate_cw:
            PORTA |= (1 << PWRON);
            g_motors.turnaround( false, a_param );
            PORTA &= ~(1 << PWRON);
            break;

        case c_cmd_rotate_ccw:
            PORTA |= (1 << PWRON);
            g_motors.turnaround( true, a_param );
            PORTA &= ~(1 << PWRON);
            break;

        case c_cmd_speed:
            g_motors.m_speed_left = a_param;
            g_motors.m_speed_right = a_param;
            break;

        case c_cmd_speed_left:
            g_motors.m_speed_left = a_param;
            if ( g_motors.m_speed_left < MIN_SPEED )
            {
                //
                g_motors.m_speed_left = MIN_SPEED;
            }
            break;

        case c_cmd_speed_right:
            g_motors.m_speed_right = a_param;
            if ( g_motors.m_speed_right < MIN_SPEED )
            {
                //
                g_motors.m_speed_right = MIN_SPEED;
            }
            break;

        default:
            g_uart.write_string_p(P("unexpected command\r\n"));
            break;
    }
}


//
ISR(PCINT2_vect)
{
    //
    g_range_meter.process_interrupt();
    //
    g_motors.process_interrupt();
}


ISR(TIMER2_COMPA_vect)
{
    // поддержка таймаутов клавиатуры
    if ( g_bt_timeouts[0] ) { --g_bt_timeouts[0]; }
    if ( g_bt_timeouts[1] ) { --g_bt_timeouts[1]; }
    if ( g_bt_timeouts[2] ) { --g_bt_timeouts[2]; }
    if ( g_bt_timeouts[3] ) { --g_bt_timeouts[3]; }
}

uint8_t get_buttons()
{
    uint8_t v_change = g_buttons;
    g_buttons = ~(PINA);
    //
    v_change ^= g_buttons;
    //
    if ( g_bt_timeouts[0] ) { v_change &= ~(1 << BT_1); }
    if ( g_bt_timeouts[1] ) { v_change &= ~(1 << BT_2); }
    if ( g_bt_timeouts[2] ) { v_change &= ~(1 << BT_3); }
    if ( g_bt_timeouts[3] ) { v_change &= ~(1 << BT_4); }
    //
    if ( v_change & (1 << BT_1) ) { g_bt_timeouts[0] = BT_TIMEOUT; }
    if ( v_change & (1 << BT_2) ) { g_bt_timeouts[1] = BT_TIMEOUT; }
    if ( v_change & (1 << BT_3) ) { g_bt_timeouts[2] = BT_TIMEOUT; }
    if ( v_change & (1 << BT_4) ) { g_bt_timeouts[3] = BT_TIMEOUT; }
    //
    return v_change;
}

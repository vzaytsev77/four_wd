#include "CUart.h"
#include <ctype.h>


ISR(USART0_RX_vect)
{
    uint8_t v = UDR0;

    //
    if ( ! g_uart.m_rx_full )
    {
        //
        g_uart.m_rx_ring[g_uart.m_rx_head++] = v;
        //
        if ( g_uart.m_rx_head == g_uart.m_rx_tail )
        {
            //
            g_uart.m_rx_full = true;
        }
        //
        g_uart.m_rx_empty = false;
    }
}


ISR(USART0_UDRE_vect)
{
    //
    if ( ! g_uart.m_tx_empty )
    {
        // если BT-модуль подключен
        if ( 0 != (PINC & (1 << BT_STATE)) )
        {
            // передаём символ
            UDR0 = g_uart.m_tx_ring[g_uart.m_tx_tail++];
        }
        else
        {
            // иначе просто его пропускаем
            ++g_uart.m_tx_tail;
        }
        //
        if ( g_uart.m_tx_head == g_uart.m_tx_tail )
        {
            //
            g_uart.m_tx_empty = true;
            //
            UCSR0B &= ~(1 << UDRIE0);
        }
    }
    else
    {
        //
        UCSR0B &= ~(1 << UDRIE0);
    }
}


//
CUart::CUart()
    : m_tx_ring{0}
    , m_tx_head(0)
    , m_tx_tail(0)
    , m_tx_empty(true)
    , m_tx_full(false)
    , m_rx_ring{0}
    , m_rx_head(0)
    , m_rx_tail(0)
    , m_rx_empty(true)
    , m_rx_full(false)
    , m_line_idx(0)
{
    // UART
    UBRR0L = (uint8_t)(F_CPU/(16*BAUD_RATE)) - 1; // 38400
    UBRR0H = (uint8_t)(((uint16_t)(F_CPU/(16*BAUD_RATE)) - 1) >> 8);
    //
    UCSR0A = 0;
    UCSR0C = 0x0E; //(1 << USBS0) + (1 << UCSZ10) + (1 << UCSZ00);
    UCSR0B = (1 << RXCIE0) + (1 << RXEN0) + (1 << TXEN0);

}


uint8_t CUart::tx_write( uint8_t * a_src, uint8_t ** a_end_ptr, uint8_t a_count )
{
    uint8_t v = 0;

    //
    cli();
    //
    while ( !m_tx_full && a_count )
    {
        //
        m_tx_ring[m_tx_head++] = *(a_src++);
        //
        ++v;
        //
        --a_count;
        //
        if ( m_tx_head == m_tx_tail )
        {
            //
            m_tx_full = true;
        }
    }
    //
    *a_end_ptr = a_src;
    //
    if ( m_tx_empty )
    {
        //
        m_tx_empty = false;
        //
        UCSR0B |= (1 << UDRIE0);
    }
    //
    sei();
    //
    return v;
}

//
void CUart::write_string( const char * a_str )
{
    //
    for ( ; *a_str; ++a_str )
    {
        //
        write_char( *a_str );
    }
}


//
void CUart::write_string_p( const char * a_str )
{
	char c;

	//
	while ( (c = pgm_read_byte(a_str++)) )
	{
        //
        write_char( c );
	}
}


//
void CUart::write_char( char a_char )
{
    //
    while ( m_tx_full )
    {
        //
        _delay_us( 1 );
    }
    //
    cli();
    //
    m_tx_ring[m_tx_head++] = (uint8_t)a_char;
    //
    if ( m_tx_head == m_tx_tail )
    {
        //
        m_tx_full = true;
    }
    //
    if ( m_tx_empty )
    {
        //
        m_tx_empty = false;
        //
        UCSR0B |= (1 << UDRIE0);
    }
    //
    sei();
}


//
bool CUart::read_char( char & a_dst )
{
    //
    if ( m_rx_empty )
    {
        //
        return false;
    }
    //
    cli();
    //
    a_dst = m_rx_ring[m_rx_tail++];
    //
    if ( m_rx_tail == m_rx_head )
    {
        //
        m_rx_empty = true;
    }
    //
    sei();
    //
    return true;
}

//
bool CUart::read_string( char * a_dst, uint8_t & a_readed, uint8_t a_limit )
{
    char c;
    //
    a_readed = 0;
    //
    while ( 0 != a_limit && read_char( c ) )
    {
        //
        if ( '\r' == c )
        {
            //
            continue;
        }
        //
        if ( '\n' == c )
        {
            //
            *a_dst = '\0';
            //
            return true;
        }
        else
        {
            //
            *a_dst++ = c;
            //
            ++a_readed;
            //
            --a_limit;
        }
    }
    //
    return false;
}

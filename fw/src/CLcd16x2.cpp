#include "CLcd16x2.h"

CLcd16x2::CLcd16x2()
{
}


//
void CLcd16x2::init()
{
    //
    _delay_ms( 16 );
    //
    write_half_cmd( 0x30 );
    //
    _delay_ms( 5 );
    //
    write_half_cmd( 0x30 );
    //
    _delay_us( 101 );
    //
    write_half_cmd( 0x30 );
    //
    _delay_us( 101 );
    //
    write_half_cmd( 0x20 );
    //
    _delay_us( 101 );
    //
    write_cmd( 0x28 );
    //
    write_cmd( 0x08 );
    //
    write_cmd( 0x01 );
    //
    write_cmd( 0x06 );
    //
    write_cmd( 0x0C );
    //
    write_cmd( 0x02 );
    //
    write_string_P("\x01HELLO!\x02SUPERCAR V1.0");
}

//
void CLcd16x2::write_cmd( uint8_t a_byte )
{
    //
    write_half_cmd( a_byte );
    write_half_cmd( a_byte << 4 );
    //
    wait_busy();
}

//
void CLcd16x2::write_half_cmd( uint8_t a_four_bits )
{
    //
    DDRB |= (1 << LCD_D7) + (1 << LCD_D6) + (1 << LCD_D5) + (1 << LCD_D4);
    //
    PORTB = (a_four_bits & 0xF0) | (PORTB & (1 << BEEP));
    //
    PORTB |= (1 << LCD_E);
    //
    _delay_us( 1 );
    //
    PORTB &= ~(1 << LCD_E);
    //
    _delay_us( 1 );
}


//
void CLcd16x2::write_data( uint8_t a_byte )
{
    //
    write_half_data( a_byte );
    write_half_data( a_byte << 4 );
    //
    wait_busy();
}


//
void CLcd16x2::write_half_data( uint8_t a_four_bits )
{
    //
    DDRB |= (1 << LCD_D7) + (1 << LCD_D6) + (1 << LCD_D5) + (1 << LCD_D4);
    //
    PORTB = (a_four_bits & 0xF0) | (PORTB & (1 << BEEP)) | (1 << LCD_RS);
    //
    PORTB |= (1 << LCD_E);
    //
    _delay_us( 1 );
    //
    PORTB &= ~(1 << LCD_E);
    //
    _delay_us( 1 );
}


//
void CLcd16x2::wait_busy()
{
    //
    DDRB &= ~((1 << LCD_D7) + (1 << LCD_D6) + (1 << LCD_D5) + (1 << LCD_D4));
    //
    PORTB = (PORTB & (1 << BEEP)) | (1 << LCD_RW);
    //
    for ( uint16_t i = 0; i < 400; ++i )
    {
        //
        PORTB |= (1 << LCD_E);
        //
        _delay_us( 1 );
        //
        uint8_t v = PINB;
        //
        PORTB &= ~(1 << LCD_E);
        //
        _delay_us( 1 );
        //
        PORTB |= (1 << LCD_E);
        //
        _delay_us( 1 );
        //
        PORTB &= ~(1 << LCD_E);
        //
        _delay_us( 1 );
        //
        if ( 0 == (v & 0x80) )
        {
            //
            break;
        }
    }
}


//
void CLcd16x2::write_char( char a_char )
{
    //
    if ( '\1' == a_char )
    {
        // начать с первой строки
        write_cmd( 0x80 );
    }
    else
    if ( '\2' == a_char )
    {
        // начать со второй строки
        write_cmd( 0xC0 );
    }
    else
    if ( '\3' == a_char )
    {
        // очистить экран
        write_cmd( 0x01 );
    }
    else
    {
        //
        write_data( (uint8_t)a_char );
    }
}


//
void CLcd16x2::write_string( const char * a_str )
{
    //
    for ( ; *a_str; ++a_str )
    {
        //
        write_char( *a_str );
    }
}


//
void CLcd16x2::write_string_p( const char * a_str )
{
	char c;

	//
	while ( (c = pgm_read_byte(a_str++)) )
	{
        //
        write_char( c );
	}
}
